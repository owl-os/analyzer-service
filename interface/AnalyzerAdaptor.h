#ifndef ANALYZER_ADAPTOR_H_
#define ANALYZER_ADAPTOR_H_

#include <string>
#include <memory>
#include <glib.h>
#include <map>
#include <set>

#include "GDBusAnalyzer.h"
#include "DBusAdaptor.hpp"

class AnalyzerAdaptor : public DBusAdaptor<ComOsOwlAnalyzer, AnalyzerAdaptor>
{
public:
    AnalyzerAdaptor();
    virtual ~AnalyzerAdaptor();

    std::string GetServiceName() const override
    {
        return "com.os.owl.analyzer";
    }

    std::string GetObjectPath() const override
    {
        return "/com/os/owl/analyzer";
    }

    ComOsOwlAnalyzer* AdaptorCreateSkeleton() const override
    {
        return com_os_owl_analyzer_skeleton_new();
    }

private:
    AnalyzerAdaptor(const AnalyzerAdaptor&) = delete;
    AnalyzerAdaptor& operator=(const AnalyzerAdaptor&) = delete;
    
};

#endif