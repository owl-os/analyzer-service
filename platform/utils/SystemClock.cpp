/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/SystemClock.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <SystemClock.hpp>

#include <chrono>

namespace android
{
    namespace SystemClock
    {

        uint64_t uptimeMillis()
        {
            auto now = std::chrono::steady_clock::now();
            auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
            return dur.count();
        }

        uint64_t elapsedRealtime()
        {
            return uptimeMillis();
        }

        uint64_t currentTimeMillis()
        {
            auto now = std::chrono::system_clock::now();
            auto dur = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
            return dur.count();
        }

    } // namespace SystemClock

} // namespace android
