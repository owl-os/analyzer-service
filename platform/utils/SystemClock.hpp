/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/SystemClock.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DIAG_SYSTEM_CLOCK_HPP_
#define DIAG_SYSTEM_CLOCK_HPP_

#include <cstdint>

namespace android
{
    namespace SystemClock
    {

        /**
         * Returns milliseconds since boot, not counting time spent in deep sleep. <b>Note:</b> This
         * value may get reset occasionally (before it would otherwise wrap around).
         *
         * @return milliseconds of non-sleep uptime since boot.
         */
        uint64_t uptimeMillis();

        /**
         * Returns milliseconds since boot.
         *
         * @return elapsed milliseconds since boot.
         */
        uint64_t elapsedRealtime();

        /**
         * Returns current wall time in milliseconds.
         *
         * @return elapsed milliseconds in wall time
         */
        uint64_t currentTimeMillis();

    } // namespace SystemClock

} // namespace android

#endif // DIAG_SYSTEM_CLOCK_HPP_
