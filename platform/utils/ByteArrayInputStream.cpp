/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/libcore/luni/src/main/java/java/io/ByteArrayInputStream.java
 */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <ByteArrayInputStream.hpp>

#include <cstring>

namespace android
{

    ByteArrayInputStream::ByteArrayInputStream(uint8_t *buf, int32_t length)
        : mBuf(buf), mPos(0), mCount(length)
    {
    }

    ByteArrayInputStream::~ByteArrayInputStream()
    {
        mBuf = nullptr;
        mPos = -1;
        mCount = -1;
    }

    /**
     * Returns the number of remaining bytes
     */
    int32_t ByteArrayInputStream::available()
    {
        return (mCount - mPos);
    }

    /**
     * Reads a single byte from the source byte array and returns it as an
     * integer in the range from 0 to 255. Returns -1 if the end of the source
     * array has been reached.
     */
    int32_t ByteArrayInputStream::read()
    {
        std::lock_guard<std::mutex> lock(mMutex);

        int32_t ret = -1;
        if (mPos < mCount)
        {
            ret = (int32_t)mBuf[mPos++];
        }

        return ret;
    }

    /**
     * Reads up to count bytes from this stream and stores them in
     * the byte array buffer starting at offset.
     * Returns the number of bytes actually read or -1 if the end of the stream
     * has been reached.
     */
    int32_t ByteArrayInputStream::read(uint8_t *buffer, int32_t offset, int32_t count)
    {
        std::lock_guard<std::mutex> lock(mMutex);

        if (mPos >= mCount)
        {
            return -1;
        }

        if (mPos + count > mCount)
        {
            count = mCount - mPos;
        }

        if (count == 0)
        {
            return 0;
        }

        std::memcpy(buffer + offset, mBuf + mPos, count);
        mPos += count;

        return count;
    }

} // namespace android
