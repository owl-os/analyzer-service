#ifndef COMMANDER_HPP_
#define COMMANDER_HPP_

#include <cstdint>
#include <string>

namespace android
{
    namespace Commander
    {
        std::string execute(std::string command);
    }
}

#endif