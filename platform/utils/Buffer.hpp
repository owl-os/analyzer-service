/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/dalvik/dx/src/com/android/dx/util/ByteArray.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DIAG_BUFFER_HPP_
#define DIAG_BUFFER_HPP_

#include <memory>
#include <vector>

namespace android
{

    class Buffer: public std::enable_shared_from_this<Buffer>
    {
    public:
        Buffer();
        Buffer(Buffer& other);
        virtual ~Buffer();

        Buffer& operator=(const Buffer& other);
        uint8_t operator[](const Buffer& other);
        void setTo(const Buffer& set_buf);
        void setTo(uint8_t* buf, int32_t len);
        void setTo(char* buf, int32_t len);
        void setTo(uint8_t* buf, int32_t offset, int32_t len);
        void setTo(char* buf, int32_t offset, int32_t len);
        void setTo(uint64_t data);
        void assignTo(uint32_t len, uint32_t val);
        void append(uint8_t* buf, int32_t len);
        void append(uint64_t data);
        void copy(uint8_t* buf, uint32_t len, uint32_t offset);
        void clear(void);
        std::vector<uint8_t>* containerPointer();
        bool isEqual(uint8_t* buf, uint32_t len);

        uint8_t* data();
        uint32_t size() const;
        bool empty();

        std::vector<uint8_t> mVec;

    private:

    };

} // namespace android

#endif // DIAG_DIAG_BUFFER_HPP_
