/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/libcore/luni/src/main/java/java/io/ByteArrayOutputStream.java
 */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <ByteArrayOutputStream.hpp>

#include <cstring>

namespace android
{

    /**
     * Constructs a new {@code ByteArrayOutputStream} with a default size of
     * {@code size} bytes. If more than {@code size} bytes are written to this
     * instance, the underlying byte array will expand.
     */
    ByteArrayOutputStream::ByteArrayOutputStream(int32_t size)
        : mPos(0), mCount(size)
    {
        mBuf = new uint8_t[size];
    }

    ByteArrayOutputStream::~ByteArrayOutputStream()
    {
        mCount = 0;
        mPos = -1;
        if (mBuf != nullptr)
        {
            delete[] mBuf;
            mBuf = nullptr;
        }
    }

    /**
     * Returns the contents of this ByteArrayOutputStream as a byte array. Any
     * changes made to the receiver after returning will not be reflected in the
     * byte array returned to the caller.
     */
    uint8_t *ByteArrayOutputStream::toByteArray()
    {
        uint8_t *newArray = new uint8_t[mPos];
        std::memcpy(newArray, mBuf, mPos);
        return newArray;
    }

    /**
     * Writes the specified byte {@code oneByte} to the OutputStream. Only the
     * low order byte of {@code oneByte} is written.
     */
    void ByteArrayOutputStream::write(int32_t oneByte)
    {
        std::lock_guard<std::mutex> lock(mMutex);

        if (mPos == mCount)
        {
            expand(1);
        }
        mBuf[mPos++] = (uint8_t)oneByte;
    }

    /**
     * Writes {@code length} bytes from the byte array {@code buffer} starting at
     * position {@code offset} to this stream.
     */
    void ByteArrayOutputStream::write(uint8_t *buffer, int32_t offset, int32_t length)
    {
        std::lock_guard<std::mutex> lock(mMutex);

        if (length == 0)
        {
            return;
        }

        expand(length);

        std::memcpy(mBuf + mPos, buffer + offset, length);
        mPos += length;
    }

    void ByteArrayOutputStream::expand(int32_t i)
    {
        /* Can the buffer handle @i more bytes, if not expand it */
        if (mPos + i <= mCount)
        {
            return;
        }

        uint8_t *newBuf = new uint8_t[(mPos + i) * 2];
        std::memcpy(newBuf, mBuf, mPos);

        delete[] mBuf;

        mBuf = newBuf;
        mCount = (mPos + i) * 2;
    }

    int32_t ByteArrayOutputStream::getCount()
    {
        return mPos;
    }

} // namespace android
