#include <Commander.hpp>
#include <LogService.h>
#include <errno.h>

#define MAX_COMMAND_SIZE    1024

namespace android
{
    namespace Commander
    {
        std::string execute(std::string command)
        {
            FILE *fp;
            std::string result = "";
            char buffer[MAX_COMMAND_SIZE] = { 0 };

            fp = popen(command.c_str(), "r");
            if(fp == NULL)
            {
                result = std::strerror(errno);
                goto err_case;
            }

            while(fgets(buffer, MAX_COMMAND_SIZE, fp) != NULL)
            {
                result += std::string(buffer);
            }
        err_case:
            pclose(fp);
            return result;
        }
    }
}