/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/util/TimeUtils.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <TimeHelper.hpp>

#include <chrono>
#include <cstring>
#include <ctime>

namespace android
{
    namespace TimeHelper
    {

        std::string formatDuration(uint64_t now)
        {
            auto ms1 = static_cast<time_t>(now / 1000);
            auto ms2 = std::chrono::milliseconds(now) % std::chrono::seconds(1);

            char mbstr[64] = {
                0,
            };
            char msec[5] = {
                0,
            };
            std::strftime(mbstr, sizeof(mbstr), "%m-%d %H:%M:%S", std::localtime(&ms1));
            std::snprintf(msec, sizeof(msec), ".%03ld", static_cast<int64_t>(ms2.count()));

            std::string ret;
            ret.append(mbstr).append(msec);
            return ret;
        }

    } // namespace TimeHelper
} // namespace android
