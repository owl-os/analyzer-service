/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Thread.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Thread.hpp>

#if !defined(TARGET_EMUL)
#include <sys/prctl.h>
#endif

#include <Tlog.hpp>

namespace android
{

    namespace
    {

        constexpr const char *const kLogTag = "InternalThread";

    } // namespace

    InternalThread::InternalThread() : InternalThread("")
    {
    }

    InternalThread::InternalThread(const std::string &name) : mName(name), mStarted(false)
    {
    }

    InternalThread::~InternalThread()
    {
        if (mThread.joinable())
        {
            mThread.join();
        }
    }

    void InternalThread::run()
    {
    }

    std::string InternalThread::getName() const
    {
        return mName;
    }

    void InternalThread::join()
    {
        mThread.join();
    }

    bool InternalThread::isAlive() const
    {
        return mHoldSelf != nullptr;
    }

    std::thread::id InternalThread::getId() const
    {
        return mThread.get_id();
    }

    bool InternalThread::start()
    {
        if (!mStarted)
        {
            mHoldSelf = shared_from_this();
            mThread = std::thread(&InternalThread::threadLoop, this, this);
            mStarted = true;
            return true;
        }
        return false;
    }

    void InternalThread::threadLoop(InternalThread *user)
    {
#if !defined(TARGET_EMUL)
        if (!mName.empty())
        {
            setThreadName(mName.c_str());
        }
#endif

        InternalThread *const self = user;
        self->run();
        self->mHoldSelf.reset();
    }

#if !defined(TARGET_EMUL)
    void InternalThread::setThreadName(const char *name)
    {
        int32_t hasAt = 0;
        int32_t hasDot = 0;
        const char *s = name;
        while (*s)
        {
            if (*s == '.')
                hasDot = 1;
            else if (*s == '@')
                hasAt = 1;
            s++;
        }
        int32_t len = s - name;
        if (len < 15 || hasAt || !hasDot)
        {
            s = name;
        }
        else
        {
            s = name + len - 15;
        }
        prctl(PR_SET_NAME, reinterpret_cast<unsigned long>(s), 0, 0, 0);
    }
#endif

} // namespace android
