/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Looper.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_LOOPER_HPP_
#define ANALYZER_LOOPER_HPP_

#include <memory>
#include <mutex>
#include <thread>

namespace android
{

    class MessageQueue;
#ifndef __UNITTEST__
    class Looper final
    {
    public:
        explicit Looper(bool quitAllowed);
        virtual ~Looper() = default;
#else
    class Looper
    {
    public:
        Looper(bool quitAllowed);
        virtual ~Looper();
#endif

        static void prepare();
        // static void prepareMainLooper();
        // static std::shared_ptr<Looper> getMainLooper();
        static void loop();
        static std::shared_ptr<Looper> myLooper();
        static std::shared_ptr<MessageQueue> myQueue();

        void quit();
        void quitSafely();
        std::thread::id getThreadId() const;
        std::shared_ptr<MessageQueue> getQueue() const;
        std::string toString() const;

    private:
        Looper(const Looper &) = delete;
        Looper &operator=(const Looper &) = delete;

        static void prepare(bool quitAllowed);

    private:
        std::shared_ptr<MessageQueue> mQueue;
        std::thread::id mThreadId;
    };

} // namespace android

#endif // ANALYZER_LOOPER_HPP_
