/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/HandlerThread.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <HandlerThread.hpp>

namespace android
{

    HandlerThread::HandlerThread() : HandlerThread("")
    {

    }

    HandlerThread::HandlerThread(const std::string &name) : InternalThread(name)
    {

    }

    void HandlerThread::run()
    {
        Looper::prepare();
        mMutex.lock();
        mLooper = Looper::myLooper();
        mCondition.notify_all();
        mMutex.unlock();
        onLooperPrepared();
        Looper::loop();
    }

    std::shared_ptr<Looper> HandlerThread::getLooper()
    {
        std::unique_lock<std::mutex> lock(mMutex);
        if (!isAlive())
        {
            return nullptr;
        }
        while (isAlive() && mLooper == nullptr)
        {
            mCondition.wait(lock);
        }
        return mLooper;
    }

    bool HandlerThread::quit()
    {
        auto looper = getLooper();
        if (looper != nullptr)
        {
            looper->quit();
            return true;
        }
        return false;
    }

    bool HandlerThread::quitSafely()
    {
        auto looper = getLooper();
        if (looper != nullptr)
        {
            looper->quitSafely();
            return true;
        }
        return false;
    }

} // namespace android
