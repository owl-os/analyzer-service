/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Thread.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_THREAD_HPP_
#define ANALYZER_THREAD_HPP_

#include <memory>
#include <string>
#include <thread>

namespace android
{

    class InternalThread : public std::enable_shared_from_this<InternalThread>
    {
    public:
        InternalThread();
        explicit InternalThread(const std::string &name);
        virtual ~InternalThread();

        virtual void run();

        std::string getName() const;
        bool start();
        void join();
        bool isAlive() const;
        std::thread::id getId() const;

    private:
        InternalThread(const InternalThread &) = delete;
        InternalThread &operator=(const InternalThread &) = delete;

        void threadLoop(InternalThread *user);

#if !defined(TARGET_EMUL)
        void setThreadName(const char *s);
#endif

    private:
        std::string mName;
        std::thread mThread;
        bool mStarted;
        std::shared_ptr<InternalThread> mHoldSelf;
    };

} // namespace android

#endif // ANALYZER_THREAD_HPP_
