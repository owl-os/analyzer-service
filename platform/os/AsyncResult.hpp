/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/AsyncResult.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_ASYNC_RESULT_HPP_
#define ANALYZER_ASYNC_RESULT_HPP_

#include <exception>
#include <memory>

#include <Message.hpp>

namespace android
{

    class AsyncResult
    {
    public:
        AsyncResult(const std::shared_ptr<void> &uo, const std::shared_ptr<void> &r, const std::shared_ptr<std::exception> &ex);
        virtual ~AsyncResult() = default;

        static std::shared_ptr<AsyncResult> forMessage(const std::shared_ptr<Message> &m, const std::shared_ptr<void> &r, const std::shared_ptr<std::exception> &ex);
        static std::shared_ptr<AsyncResult> forMessage(const std::shared_ptr<Message> &m);

        template<typename T>
        static std::shared_ptr<T> getResult(const std::shared_ptr<Message> &m, std::shared_ptr<std::exception> &ex)
        {
            auto ar = std::static_pointer_cast<AsyncResult>(m->obj);
            if(ar == nullptr)
            {
                ex = nullptr;
                return nullptr;
            }

            if((ex = ar->excption) != nullptr)
            {
                return nullptr;
            }
            else
            {
                return std::static_pointer_cast<T>(ar->result);
            }
        }

        template<typename T>
        static std::shared_ptr<T> getUserObj(const std::shared_ptr<Message> &m)
        {
            auto ar = std::static_pointer_cast<AsyncResult>(m->obj);
            if((ar == nullptr) || (ar->userObj == nullptr))
            {
                return nullptr;
            }
            else
            {
                return std::static_pointer_cast<T>(ar->userObj);
            }
        }

    public:
        std::shared_ptr<void> userObj;
        std::shared_ptr<void> result;
        std::shared_ptr<std::exception> excption;
    };

} // namespace telephony

#endif // ANALYZER_ASYNC_RESULT_HPP_
