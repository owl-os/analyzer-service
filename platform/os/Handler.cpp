/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Handler.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Handler.hpp>

#include <cassert>

#include <Looper.hpp>
#include <Message.hpp>
#include <MessageQueue.hpp>

#include <SystemClock.hpp>
#include <Tlog.hpp>

namespace android
{

    namespace
    {

        constexpr const char *const kLogTag = "Handler";

    } // namespace

    Handler::Handler()
    {
        mLooper = Looper::myLooper();
        assert(mLooper != nullptr);
        mQueue = mLooper->getQueue();
    }

    Handler::Handler(const std::shared_ptr<Looper> &looper)
        : mLooper(looper),
          mQueue(looper->getQueue())
    {
    }

    void Handler::handleMessage(const std::shared_ptr<Message> &msg)
    {
    }

    void Handler::dispatchMessage(const std::shared_ptr<Message> &msg)
    {
        handleMessage(msg);
    }

    std::shared_ptr<Message> Handler::obtainMessage()
    {
        return Message::obtain(shared_from_this());
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what)
    {
        return Message::obtain(shared_from_this(), what);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1)
    {
        return Message::obtain(shared_from_this(), what, arg1);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1, int32_t arg2)
    {
        return Message::obtain(shared_from_this(), what, arg1, arg2);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1, int32_t arg2, int32_t arg3)
    {
        return Message::obtain(shared_from_this(), what, arg1, arg2, arg3);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, const std::shared_ptr<void> &obj)
    {
        return Message::obtain(shared_from_this(), what, obj);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1, const std::shared_ptr<void> &obj)
    {
        return Message::obtain(shared_from_this(), what, arg1, obj);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1, int32_t arg2, const std::shared_ptr<void> &obj)
    {
        return Message::obtain(shared_from_this(), what, arg1, arg2, obj);
    }

    std::shared_ptr<Message> Handler::obtainMessage(int32_t what, int32_t arg1, int32_t arg2, int32_t arg3, const std::shared_ptr<void> &obj)
    {
        return Message::obtain(shared_from_this(), what, arg1, arg2, arg3, obj);
    }

    bool Handler::sendMessage(const std::shared_ptr<Message> &msg)
    {
        return sendMessageDelayed(msg, 0);
    }

    bool Handler::sendEmptyMessage(int32_t what)
    {
        return sendEmptyMessageDelayed(what, 0);
    }

    bool Handler::sendEmptyMessageDelayed(int32_t what, uint64_t delayMillis)
    {
        auto msg = Message::obtain();
        msg->what = what;
        return sendMessageDelayed(msg, delayMillis);
    }

    bool Handler::sendEmptyMessageAtTime(int32_t what, uint64_t uptimeMillis)
    {
        auto msg = Message::obtain();
        msg->what = what;
        return sendMessageAtTime(msg, uptimeMillis);
    }

    bool Handler::sendMessageDelayed(const std::shared_ptr<Message> &msg, uint64_t delayMillis)
    {
        return sendMessageAtTime(msg, SystemClock::uptimeMillis() + delayMillis);
    }

    bool Handler::sendMessageAtTime(const std::shared_ptr<Message> &msg, uint64_t uptimeMillis)
    {
        if (!mQueue)
        {
            //TLOGW(kLogTag, "RuntimeException: sendMessageAtTime() called with no mQueue");
            return false;
        }
        return enqueueMessage(mQueue, msg, uptimeMillis);
    }

    bool Handler::sendMessageAtFrontOfQueue(const std::shared_ptr<Message> &msg)
    {
        if (!mQueue)
        {
            //TLOGW(kLogTag, "RuntimeException: sendMessageAtFrontOfQueue() called with no mQueue");
            return false;
        }
        return enqueueMessage(mQueue, msg, 0);
    }

    void Handler::removeMessages(int32_t what)
    {
        mQueue->removeMessages(shared_from_this(), what, nullptr);
    }

    void Handler::removeMessages(int32_t what, const std::shared_ptr<void> &obj)
    {
        mQueue->removeMessages(shared_from_this(), what, obj);
    }

    void Handler::removeMessages(int32_t what, int32_t arg1)
    {
        mQueue->removeMessages(shared_from_this(), what, arg1);
    }

    bool Handler::hasMessages(int32_t what)
    {
        return mQueue->hasMessages(shared_from_this(), what, nullptr);
    }

    bool Handler::hasMessages(int32_t what, const std::shared_ptr<void> &obj)
    {
        return mQueue->hasMessages(shared_from_this(), what, obj);
    }

    std::shared_ptr<Looper> Handler::getLooper() const
    {
        return mLooper;
    }

    bool Handler::enqueueMessage(const std::shared_ptr<MessageQueue> &queue, const std::shared_ptr<Message> &msg, uint64_t uptimeMillis)
    {
        msg->target = shared_from_this();
        return queue->enqueueMessage(msg, uptimeMillis);
    }

#ifdef __DEBUG__
    void Handler::setDebugName(const std::string &name)
    {
        mDebugName = name;
    }

    std::string Handler::getDebugName() const
    {
        return mDebugName;
    }
#endif

} // namespace telephony
