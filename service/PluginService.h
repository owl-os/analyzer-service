#ifndef PLUGIN_SERVICE_H
#define PLUGIN_SERVICE_H

#include <memory>

#include "Handler.hpp"
#include "HandlerThread.hpp"
#include "Looper.hpp"
#include "Message.hpp"
#include "Error.hpp"
#include "AnalyzerService.h"

class AnalyzerService;
class PluginService
{
public:
    PluginService(AnalyzerService& parent);
    virtual ~PluginService();

    error_t start();

    class PluginThread final : public android::InternalThread
    {
    public:
        PluginThread(PluginService& parent);
        ~PluginThread();
    
        virtual void run();

    private:
        volatile bool mLoop;
        PluginService& mParent;
    };

private:
    AnalyzerService& mParent;
    std::shared_ptr<android::InternalThread> mPluginThread;
};

#endif