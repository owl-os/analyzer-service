#include <sys/signalfd.h>
#include <csignal>
#include <linux/netlink.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstring>
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <poll.h>

#include "PluginService.h"
#include "LogService.h"

PluginService::PluginService(AnalyzerService& parent)
    : mParent(parent)
    , mPluginThread(std::make_shared<PluginThread>(*this))
{

}

PluginService::~PluginService()
{

}

error_t PluginService::start()
{
    if(!mPluginThread->start())
    {
        LOG_ERROR("Cannot start PluginService");
        return E_ERROR;
    }

    LOG_INFO("PluginService started");
    return E_OK;
}

PluginService::PluginThread::PluginThread(PluginService& parent)
    : InternalThread("PluginService.PluginThread")
    , mParent(parent)
    , mLoop(true)
{

}

PluginService::PluginThread::~PluginThread()
{
    mLoop = false;
}

void PluginService::PluginThread::run()
{
    struct sockaddr_nl addr = {0};
    char buffer[4096];
    sigset_t signal_set;
    struct signalfd_siginfo signal_info;
    struct pollfd pfd[2];
    int ret_poll;
    ssize_t n;

    // Set signals we want to catch
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGINT);

    // Change the signal mask and check
    if (sigprocmask(SIG_BLOCK, &signal_set, nullptr) < 0) {
        LOG_ERROR("Error while sigprocmask(): %s", strerror(errno));
        return;
    }
    // Get a signal file descriptor
    pfd[0].fd = signalfd(-1, &signal_set, 0);
    // Check the signal file descriptor
    if (pfd[0].fd < 0) {
        LOG_ERROR("Error while signalfd(): %s", strerror(errno));
        return;
    }

    // Create a netlink socket
    pfd[1].fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_KOBJECT_UEVENT);
    if (pfd[1].fd < 0) {
        LOG_ERROR("Netlink socket create failed: %s", strerror(errno));
        return;
    }

    addr.nl_family = AF_NETLINK;
    addr.nl_pid = getpid();
    addr.nl_groups = 2;

    if (bind(pfd[1].fd, (struct sockaddr *) &addr, sizeof(addr))) {
        LOG_ERROR("Netlink socket bind() failed: %s", strerror(errno));
        return;
    }

    pfd[0].events = POLLIN;
    pfd[1].events = POLLIN;

    while(mLoop)
    {
        ret_poll = poll(pfd, 2, -1);
        if (ret_poll < 0) {
            LOG_ERROR("SystemMaster::execute() -> "
                            "Error while poll(): %s", strerror(errno));
            return;
        }
        // True, if a signal from the operating system was sent to this process
        if (pfd[0].revents & POLLIN) {
            // Get the signal
            n = read(pfd[0].fd, &signal_info, sizeof(signal_info));
            // True, if an error occurred while getting the signal
            if (n == -1) {
                LOG_ERROR("Error while read() on signal pipe: %s", strerror(errno));
            }
            // Check, if we are really interested in the caught signal
            if ((signal_info.ssi_signo == SIGTERM) || (signal_info.ssi_signo == SIGINT)) {
                LOG_INFO("(SIGTERM | SIGINT) signal received");
            }
            break;
        }
        // True, if a netlink message is available
        if (pfd[1].revents & POLLIN) {
            std::string message;

            n = recv(pfd[1].fd, &buffer, sizeof(buffer), 0);

            for (int i = 0; i < n; ++i) {
                if (buffer[i] == 0) message += '\n';
                else if (buffer[i] > 33 && buffer[i] < 126) message += buffer[i];
            }

            LOG_INFO("Event: %s", message.c_str());
        }
    }

    // Close both file descriptors
    close(pfd[0].fd);
    close(pfd[1].fd);

    LOG_ERROR("PluginService exit unexpectedly");
}
