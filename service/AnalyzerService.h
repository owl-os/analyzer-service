#ifndef ANALYZER_SERVICE_H
#define ANALYZER_SERVICE_H

#include <memory>

#include "Handler.hpp"
#include "HandlerThread.hpp"
#include "Looper.hpp"
#include "Message.hpp"
#include "Error.hpp"
#include "AnalyzerAdaptor.h"
#include "PluginService.h"

class PluginService;
class AnalyzerService
{
public:
    int32_t kKickWatchDog = 0;
    int32_t kWatchDogInterval = 20000;
    AnalyzerService();
    virtual ~AnalyzerService();

    virtual bool onInit();
    virtual void instantiate();
    virtual error_t onStart();
    virtual error_t onStop();

    class WatchDogHandler final : public android::Handler
    {
    public:
        WatchDogHandler(const std::shared_ptr<android::Looper> &looper, AnalyzerService &service);
        virtual ~WatchDogHandler();

        void handleMessage(const std::shared_ptr<android::Message> &msg) override;
    private:
        AnalyzerService& mService;
    };

private:
    std::shared_ptr<android::HandlerThread> mWatchDogThread;
    std::shared_ptr<AnalyzerAdaptor> mAnalyzerAdaptor;
    std::shared_ptr<android::Handler> mWatchDogHandler;
    std::shared_ptr<PluginService> mPluginService;
};

#endif