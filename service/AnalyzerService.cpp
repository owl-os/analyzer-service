#include <glib.h>

#include "LogService.h"
#include "AnalyzerService.h"
#include "Commander.hpp"

AnalyzerService::AnalyzerService()
{

}

AnalyzerService::~AnalyzerService()
{

}

bool AnalyzerService::onInit()
{
    LOG_INFO("onInit");

    return true;
}

void AnalyzerService::instantiate()
{
    LOG_INFO("instantiate");
}

error_t AnalyzerService::onStart()
{
    LOG_INFO("Onstart AnalyzerService");
    error_t ret = E_OK;

    mWatchDogThread = std::make_shared<android::HandlerThread>("WatchDogThread");
    mWatchDogThread->start();
    mWatchDogHandler = std::make_shared<WatchDogHandler>(mWatchDogThread->getLooper(), *this);
    mWatchDogHandler->sendEmptyMessage(kKickWatchDog);

    mAnalyzerAdaptor = std::make_shared<AnalyzerAdaptor>();
    mAnalyzerAdaptor->RegisterServiceToBus();

    mPluginService = std::make_shared<PluginService>(*this);
    mPluginService->start();

    GMainLoop* loop = g_main_loop_new(NULL, false);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);

    mAnalyzerAdaptor->UnregisterServiceFromBus();

    return ret;
}

error_t AnalyzerService::onStop()
{
    return E_OK;
}

AnalyzerService::WatchDogHandler::WatchDogHandler(const std::shared_ptr<android::Looper> &looper, AnalyzerService &service)
    : android::Handler(looper), mService(service)
{
}

AnalyzerService::WatchDogHandler::~WatchDogHandler()
{   
}

void AnalyzerService::WatchDogHandler::handleMessage(const std::shared_ptr<android::Message> &msg)
{
    if(msg->what == mService.kKickWatchDog)
    {
        LOG_INFO("CPU usage: %s", android::Commander::execute("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage \"%\"}'").c_str());
        LOG_INFO("RAM usage: %s", android::Commander::execute("free | grep Mem | awk '{usage = $3/$2 * 100.0 } END { print usage \"%\"}'").c_str());
        sendEmptyMessageDelayed(mService.kKickWatchDog, mService.kWatchDogInterval);
    }
    else
    {
        LOG_ERROR("Invalid Message [%d]", msg->what);
    }
}