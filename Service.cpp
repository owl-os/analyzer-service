#include <stdio.h>
#include <memory>
#include "LogService.h"
#include "AnalyzerService.h"

int main()
{
    LogService::getInstance()->registerService(std::make_shared<Logger>());
    LOG_INFO("Init Analyzer Service main");;

    AnalyzerService *service = new AnalyzerService();
    service->instantiate();
    service->onInit();
    service->onStart();

    LOG_ERROR("Analyzer Service main stopped unexpectedly");
    return 1;
}